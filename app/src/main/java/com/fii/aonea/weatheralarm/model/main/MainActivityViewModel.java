package com.fii.aonea.weatheralarm.model.main;

import android.app.AlarmManager;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.NonNull;

import com.fii.aonea.weatheralarm.data.model.Alarm;
import com.fii.aonea.weatheralarm.data.repository.AlarmRepository;

import java.util.List;

public class MainActivityViewModel extends ViewModel {

    private AlarmRepository mAlarmRepository;

    private LiveData<List<Alarm>> mAlarms;

    public MainActivityViewModel() {

    }

    public MainActivityViewModel(@NonNull Application application) {
        mAlarmRepository = new AlarmRepository(application);
        mAlarms = mAlarmRepository.getAlarms();
    }

    public LiveData<List<Alarm>> getAlarms() {
        return mAlarms;
    }

    public void deleteAllAlarms() {
        mAlarmRepository.deleteAll();
    }

}
