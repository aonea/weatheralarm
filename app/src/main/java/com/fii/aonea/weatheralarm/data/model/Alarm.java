package com.fii.aonea.weatheralarm.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "alarms")
public class Alarm {
    @PrimaryKey
    @NonNull
    private String mName;

    private int mHour;
    private int mMinutes;
    private boolean isActive;
    private boolean isWeatherReportOn;
    private long mRepeat;
    private String mGoal;
    private boolean requiresSmileSelfie;

    public Alarm(String mName, int mHour, int mMinutes, boolean isActive, boolean isWeatherReportOn,
                 long repeat, String goal, boolean requiresSmileSelfie) {
        this.mName = mName;
        this.mHour = mHour;
        this.mMinutes = mMinutes;
        this.isActive = isActive;
        this.isWeatherReportOn = isWeatherReportOn;
        this.mRepeat = repeat;
        this.mGoal = goal;
        this.requiresSmileSelfie = requiresSmileSelfie;
    }

    public String getName() {
        return mName;
    }

    public int getHour() {
        return mHour;
    }

    public int getMinutes() {
        return mMinutes;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public void setHour(int mHour) {
        this.mHour = mHour;
    }

    public void setMinutes(int mMinutes) {
        this.mMinutes = mMinutes;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isWeatherReportOn() {
        return isWeatherReportOn;
    }

    public void setWeatherReportOn(boolean weatherReportOn) {
        isWeatherReportOn = weatherReportOn;
    }

    public long getRepeat() {
        return mRepeat;
    }

    public void setRepeat(long repeat) {
        this.mRepeat = repeat;
    }

    public String getGoal() {
        return mGoal;
    }

    public void setGoal(String mGoal) {
        this.mGoal = mGoal;
    }

    public boolean requiresSmileSelfie() {
        return requiresSmileSelfie;
    }

    public void setRequiresSmileSelfie(boolean requiresSmileSelfie) {
        this.requiresSmileSelfie = requiresSmileSelfie;
    }
}
