package com.fii.aonea.weatheralarm.data.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.fii.aonea.weatheralarm.data.dao.AlarmDao;
import com.fii.aonea.weatheralarm.data.database.Database;
import com.fii.aonea.weatheralarm.data.http.RequestManager;
import com.fii.aonea.weatheralarm.data.model.Alarm;
import com.fii.aonea.weatheralarm.util.Util;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

public class AlarmRepository {
    private Database database;
    private AlarmDao alarmDao;
    private LiveData<List<Alarm>> alarmList;
    private MutableLiveData<String> mLastWeatherReport;

    public AlarmRepository(Application application) {
        database = Database.getInstance(application);
        alarmDao = database.alarmDao();
        alarmList = alarmDao.getAllAlarms();
    }

    public AlarmRepository(Context context) {
        database = Database.getInstance(context.getApplicationContext());
        alarmDao = database.alarmDao();
        alarmList = alarmDao.getAllAlarms();
    }


    public LiveData<List<Alarm>> getAlarms() {
        return alarmList;
    }

    public LiveData<String> getLastWeatherReport(double lat, double lon) {
        try {

            RequestManager.getCurrentConditions(lon, lat, new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    Log.e("AlarmRepository", "Big Failure");
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    String result = response.body().string();
                    try {
                        mLastWeatherReport.postValue(
                                Util.buildReportFromJson(result)
                        );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (mLastWeatherReport == null) {
            mLastWeatherReport = new MutableLiveData<>();
            mLastWeatherReport.setValue("No Data");
            return mLastWeatherReport;
        }

        return mLastWeatherReport;
    }

    public void insert(Alarm alarm) {
        InsertAlarmTask insertAlarmTask = new InsertAlarmTask(alarmDao);
        insertAlarmTask.execute(alarm);
    }

    public void deleteAll() {
        DeleteAllAlarmsTask deleteAllAlarmsTask = new DeleteAllAlarmsTask(alarmDao);
        deleteAllAlarmsTask.execute();
    }

    public void setActive(String name, Boolean isActive) {
        Runnable runnable = () -> alarmDao.setActive(name, isActive);

        AsyncTask.execute(runnable);
    }

    public void setWeatherReport(String name, boolean bool) {
        Runnable runnable = () -> alarmDao.setWeatherReport(name, bool);

        AsyncTask.execute(runnable);
    }

    static class InsertAlarmTask extends AsyncTask<Alarm, Void, Void> {
        private AlarmDao mAlarmDao;

        InsertAlarmTask(AlarmDao alarmDao) {
            this.mAlarmDao = alarmDao;
        }

        @Override
        protected Void doInBackground(Alarm... alarms) {
            for (Alarm alarm : alarms) {
                mAlarmDao.insert(alarm);
            }
            return null;
        }
    }

    static class DeleteAllAlarmsTask extends AsyncTask<Void, Void, Void> {
        private AlarmDao mAlarmDao;

        public DeleteAllAlarmsTask(AlarmDao mAlarmDao) {
            this.mAlarmDao = mAlarmDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAlarmDao.dropAlarmsTable();
            return null;
        }
    }
}
