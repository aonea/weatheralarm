package com.fii.aonea.weatheralarm.data.repository

import android.graphics.Bitmap
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions

class FacialRecognitionRepository {

    var smileProbability : Float = 0.0f;

    fun getSmileProbabilityFromBitmap(bitmap: Bitmap) {
        val highAccuracyOpts = FirebaseVisionFaceDetectorOptions.Builder()
                .setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
                .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                .build()

        val image = FirebaseVisionImage.fromBitmap(bitmap)

        val detector = FirebaseVision.getInstance().getVisionFaceDetector(highAccuracyOpts)

        val result = detector.detectInImage(image)
                .addOnSuccessListener { faces ->
                    run {
                        smileProbability = faces[0].smilingProbability
                    }
                }
    }
}