package com.fii.aonea.weatheralarm.system.media;

import android.content.Context;
import android.speech.tts.TextToSpeech;

public class TextToSpeechWrapper {
    private TextToSpeech mTextToSpeech;

    public TextToSpeechWrapper(Context context, TextToSpeech.OnInitListener callback) {
        this.mTextToSpeech = new TextToSpeech(context, callback);
    }

    public void speak(String toSpeak) {
        mTextToSpeech.speak(
                toSpeak,
                TextToSpeech.QUEUE_FLUSH,
                null
        );
    }

    public void clear() {
        mTextToSpeech.stop();
        mTextToSpeech.shutdown();
    }
}
