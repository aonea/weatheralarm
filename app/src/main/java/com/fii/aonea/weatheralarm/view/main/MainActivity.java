package com.fii.aonea.weatheralarm.view.main;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.fii.aonea.weatheralarm.R;
import com.fii.aonea.weatheralarm.data.model.Alarm;
import com.fii.aonea.weatheralarm.model.main.MainActivityViewModel;
import com.fii.aonea.weatheralarm.view.insert.InsertAlarmActivity;
import com.fii.aonea.weatheralarm.view.main.recyclerview.AlarmListAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    MainActivityViewModel mViewModel;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]
                            {
                                    Manifest.permission.LOCATION_HARDWARE,
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION
                            },
                    1);
        }


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), InsertAlarmActivity.class);
            startActivity(intent);
        });

        //TODO ????
        mViewModel = new MainActivityViewModel(getApplication());
//        mViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        //for dev purposes
        mViewModel.deleteAllAlarms();

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        final AlarmListAdapter alarmListAdapter = new AlarmListAdapter(this.getApplicationContext());
        recyclerView.setAdapter(alarmListAdapter);

        mViewModel.getAlarms().observe(this, alarmListAdapter::setAlarms);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
