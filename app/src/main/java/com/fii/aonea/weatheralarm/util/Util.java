package com.fii.aonea.weatheralarm.util;

import org.json.JSONException;
import org.json.JSONObject;

public class Util {

    private static String lastLocation = "";

    public static String buildReportFromJson(String jsonString) throws JSONException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello, Andrei\n");

        JSONObject jsonObject = new JSONObject(jsonString);
        JSONObject jsonWeather = (JSONObject) jsonObject.getJSONArray("weather").get(0);
        lastLocation = jsonObject.getString("name");

        String description = jsonWeather.getString("description");
        stringBuilder.append("Main description is: " + description + ".\n");

        JSONObject jsonMain = jsonObject.getJSONObject("main");
        double temp = kelvinToCelsius(jsonMain.getDouble("temp"));
        double humidity = jsonMain.getDouble("humidity");
        stringBuilder.append("Temperature outside is: " + String.valueOf((int) temp) + ".\n");
        stringBuilder.append("Humidity is: " + String.valueOf(humidity) + ".\n");

        return stringBuilder.toString();
    }

    public static double kelvinToCelsius(double kelvin) {
        return kelvin - 273.15f;
    }

    public static String getLastLocation() {
        return lastLocation;
    }
}
