package com.fii.aonea.weatheralarm.system.vibration;

import android.content.Context;
import android.os.Vibrator;

public class VibrationManager {
    private final long[] STANDARD_PATTERN= new long[]{0, 100, 1000};

    private Vibrator mVibrator;

    public VibrationManager(Context context) {
        this.mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public void vibrateIndefinetly() {
        mVibrator.vibrate(STANDARD_PATTERN, 0);
    }

    public void stopVibrating() {
        mVibrator.cancel();
    }

}
