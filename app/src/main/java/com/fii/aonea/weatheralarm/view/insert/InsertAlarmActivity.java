package com.fii.aonea.weatheralarm.view.insert;

import android.app.AlarmManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import com.fii.aonea.weatheralarm.R;
import com.fii.aonea.weatheralarm.data.model.Alarm;
import com.fii.aonea.weatheralarm.model.insert.InsertAlarmActivityViewModel;

public class InsertAlarmActivity extends AppCompatActivity {

    private InsertAlarmActivityViewModel mViewModel;

    private Alarm mAlarm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_alarm);
        mViewModel = new InsertAlarmActivityViewModel(getApplication());

        Button btnSave = (Button) findViewById(R.id.btnSave);
        RadioGroup radioGroup = findViewById(R.id.rgIntervalGroup);
        btnSave.setOnClickListener((l) -> {
            long intervalRepeat = 60 * 1000;

            switch(radioGroup.getCheckedRadioButtonId()) {
                case R.id.rbIntervalDay:
                    intervalRepeat = AlarmManager.INTERVAL_DAY;
                    break;
                case R.id.rbIntervalHalfHour:
                    intervalRepeat = AlarmManager.INTERVAL_HALF_HOUR;
                    break;
                case R.id.rbIntervalHour:
                    intervalRepeat = AlarmManager.INTERVAL_HOUR;
                    break;
            }
            mAlarm = new Alarm(
                    ((EditText) findViewById(R.id.etName)).getText().toString(),
                    ((TimePicker) findViewById(R.id.timePicker)).getHour(),
                    ((TimePicker) findViewById(R.id.timePicker)).getMinute(),
                    true,
                    true,
                    intervalRepeat,
                    ((EditText) findViewById(R.id.etGoal)).getText().toString(),
                    ((CheckBox) findViewById(R.id.cbDismissOnSmile)).isChecked()
            );

            mViewModel.insert(mAlarm);

            this.onBackPressed();
        });
    }
}
