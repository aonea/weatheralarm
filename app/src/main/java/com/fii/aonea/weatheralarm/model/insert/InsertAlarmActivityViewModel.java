package com.fii.aonea.weatheralarm.model.insert;

import android.app.Application;
import android.arch.lifecycle.ViewModel;

import com.fii.aonea.weatheralarm.data.model.Alarm;
import com.fii.aonea.weatheralarm.data.repository.AlarmRepository;

public class InsertAlarmActivityViewModel extends ViewModel {

    private AlarmRepository mAlarmRepository;

    public InsertAlarmActivityViewModel() {

    }

    public InsertAlarmActivityViewModel(Application application) {
        this.mAlarmRepository = new AlarmRepository(application);
    }

    public void insert(Alarm alarm) {
        mAlarmRepository.insert(alarm);
    }
}
