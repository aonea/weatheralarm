package com.fii.aonea.weatheralarm.view.main.recyclerview;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.fii.aonea.weatheralarm.R;
import com.fii.aonea.weatheralarm.data.model.Alarm;
import com.fii.aonea.weatheralarm.data.repository.AlarmRepository;
import com.fii.aonea.weatheralarm.system.alarm.AlarmScheduler;

import java.util.List;

public class AlarmListAdapter extends RecyclerView.Adapter<AlarmListAdapter.AlarmViewHolder> {
    private AlarmRepository mRepository;

    List<Alarm> mAlarms;
    LayoutInflater mInflater;
    Context mContext;

    public AlarmListAdapter(Context mContext) {
        this.mContext = mContext;
        this.mRepository = new AlarmRepository(mContext);
    }

    @NonNull
    @Override
    public AlarmListAdapter.AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View alarmView = layoutInflater.inflate(R.layout.item_layout_alarm, viewGroup, false);

        return new AlarmViewHolder(alarmView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmListAdapter.AlarmViewHolder alarmViewHolder, int i) {
        Alarm alarm = mAlarms.get(i);

        TextView tvName = alarmViewHolder.alarmNameTextView;
        TextView tvHour = alarmViewHolder.hourTextView;
        TextView tvMinute = alarmViewHolder.minuteTextView;
        Switch checkBox = alarmViewHolder.checkBox;
        ImageButton btnWeatherReport = alarmViewHolder.toggleButton;
        ImageView ivSmile = alarmViewHolder.ivSmile;

        if (!alarm.requiresSmileSelfie()) {
            ivSmile.setVisibility(View.GONE);
        }

        tvName.setText(alarm.getName());
        tvHour.setText(String.valueOf(alarm.getHour()));
        tvMinute.setText(String.valueOf(alarm.getMinutes()));
        checkBox.setChecked(alarm.isActive());
        btnWeatherReport.setImageDrawable(
                alarm.isWeatherReportOn() ?
                        mContext.getResources().getDrawable(R.drawable.ic_weather_report_on) :
                        mContext.getResources().getDrawable(R.drawable.ic_weather_report)
                );

        AlarmScheduler.scheduleAlarm(mContext, alarm);
        checkBox.setOnClickListener(view -> {
            mRepository.setActive(alarm.getName(), !alarm.isActive());
            AlarmScheduler.scheduleAlarm(mContext, alarm);
        });
        btnWeatherReport.setOnClickListener((l) -> {
            mRepository.setWeatherReport(alarm.getName(), !alarm.isWeatherReportOn());
            btnWeatherReport.setImageDrawable(
                    alarm.isWeatherReportOn() ?
                            mContext.getResources().getDrawable(R.drawable.ic_weather_report_on) :
                            mContext.getResources().getDrawable(R.drawable.ic_weather_report)
            );
        });
    }

    @Override
    public int getItemCount() {
        if(mAlarms == null) {
            return 0;
        }

        return mAlarms.size();
    }

    public void setAlarms(List<Alarm> alarms) {
        this.mAlarms = alarms;
        notifyDataSetChanged();
    }


    class AlarmViewHolder extends RecyclerView.ViewHolder {
        TextView alarmNameTextView;
        TextView hourTextView;
        TextView minuteTextView;
        Switch checkBox;
        ImageButton toggleButton;
        ImageView ivSmile;

        AlarmViewHolder(View itemView) {
            super(itemView);
            this.alarmNameTextView = itemView.findViewById(R.id.tvAlarmName);
            this.hourTextView = itemView.findViewById(R.id.tvHour);
            this.minuteTextView = itemView.findViewById(R.id.tvMinutes);
            this.checkBox = itemView.findViewById(R.id.alarm_switch);
            this.toggleButton = itemView.findViewById(R.id.btnWeatherReport);
            this.ivSmile = itemView.findViewById(R.id.ivSmile);
        }
    }

}
