package com.fii.aonea.weatheralarm.model.alarm;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.graphics.Bitmap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fii.aonea.weatheralarm.data.repository.AlarmRepository;
import com.fii.aonea.weatheralarm.data.repository.FacialRecognitionRepository;
import com.fii.aonea.weatheralarm.data.repository.SemanticsRepository;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;

import java.util.List;
import java.util.concurrent.ExecutionException;

import io.cortical.retina.rest.ApiException;

public class AlarmActivityViewModel extends ViewModel {

    private AlarmRepository mAlarmRepository;
    private SemanticsRepository mSemanticsRepository;
    private FacialRecognitionRepository mFacialRecognitionRepository;

    private LiveData<String> mWeatherReport;
    private MutableLiveData<Float> mSmileProbability;

    public AlarmActivityViewModel(Application application) {
        mAlarmRepository = new AlarmRepository(application);
        mSemanticsRepository = new SemanticsRepository();
        mFacialRecognitionRepository = new FacialRecognitionRepository();
    }

    public void fetchWeatherReport(double lat, double lon) {
        mWeatherReport = mAlarmRepository.getLastWeatherReport(lat, lon);
    }

    public LiveData<String> getWeatherReport() {
        return mWeatherReport;
    }

    public LiveData<Float> getSmileProbability() {
        mSmileProbability = new MutableLiveData<>();
        mSmileProbability.setValue(mFacialRecognitionRepository.getSmileProbability());
        return mSmileProbability;
    }

    public boolean areStringsSimilar(String s, String p) {
        try {
            return mSemanticsRepository.areStringsSimilar(s, p);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void fetchSmileProbability(Bitmap bitmap) {
        mFacialRecognitionRepository.getSmileProbabilityFromBitmap(bitmap);
    }

}
