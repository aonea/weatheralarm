package com.fii.aonea.weatheralarm.data.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.fii.aonea.weatheralarm.data.dao.AlarmDao;
import com.fii.aonea.weatheralarm.data.model.Alarm;

@android.arch.persistence.room.Database(entities = {Alarm.class}, version = 3)
public abstract class Database extends RoomDatabase {

    public abstract AlarmDao alarmDao();

    //Singleton
    private static Database instance;

    public static Database getInstance(final Context context) {
        if (instance == null) {
            synchronized (Database.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(
                            context.getApplicationContext(),
                            Database.class, "alarm_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }

        return instance;
    }
}
