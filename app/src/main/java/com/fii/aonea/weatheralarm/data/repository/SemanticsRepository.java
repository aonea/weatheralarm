package com.fii.aonea.weatheralarm.data.repository;

import android.os.AsyncTask;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.concurrent.ExecutionException;

import io.cortical.retina.client.LiteClient;
import io.cortical.retina.rest.ApiException;

public class SemanticsRepository {
    private static final double SIMILARITY_THRESHOLD = 0.33;

    public boolean areStringsSimilar(String s, String p)
            throws JsonProcessingException, ApiException,
            ExecutionException, InterruptedException {
        double similarity = new ComparatorTask().execute(s, p).get();
        return similarity >= SIMILARITY_THRESHOLD;
    }

    private static class ComparatorTask extends AsyncTask<String, Void, Double> {
        private static final String API_KEY = "99c80820-8e97-11e9-8f72-af685da1b20e";

        public static double result;

        @Override
        protected Double doInBackground(String... strings) {
            LiteClient lite = new io.cortical.retina.client.LiteClient(API_KEY);
            double similarity = 0;
            try {
                similarity = lite.compare(strings[0], strings[1]);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (ApiException e) {
                e.printStackTrace();
            }

            return similarity;
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            result = aDouble;
        }
    }
}
