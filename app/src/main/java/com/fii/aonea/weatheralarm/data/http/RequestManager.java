package com.fii.aonea.weatheralarm.data.http;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;

public class RequestManager {

    private final static String API_KEY = "d352e3bd8f081e0fe49d5b40e11fa29c";
    private final static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?";

    private static OkHttpClient client = new OkHttpClient();

    public static void getCurrentConditions(double lon, double lat, Callback callback) throws IOException {
        String queryUrl = BASE_URL + "lat=" + lat + "&lon=" + lon + "&appid=" + API_KEY;

        Request request = new Request.Builder()
                .url(queryUrl)
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
    }
}
