package com.fii.aonea.weatheralarm.view.alarm;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fii.aonea.weatheralarm.R;
import com.fii.aonea.weatheralarm.model.alarm.AlarmActivityViewModel;
import com.fii.aonea.weatheralarm.system.location.LocationManagerWrapper;
import com.fii.aonea.weatheralarm.system.media.TextToSpeechWrapper;
import com.fii.aonea.weatheralarm.system.vibration.VibrationManager;
import com.fii.aonea.weatheralarm.util.Util;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.util.List;

//TODO: replace multiple if No Data checks with boolean areUpdatesAvailable in viewmodel
public class AlarmActivity extends AppCompatActivity {

    private static final int TAKE_SELFIE = 1;
    private AlarmActivityViewModel mViewModel;
    private TextView tvWeatherReport;
    private TextView tvLocation;
    private Button btnSubmit;
    private TextView tvSmileProbability;
    private EditText etEnteredGoal;
    private Button btnSelfie;
    private TextToSpeechWrapper mTts;
    private boolean shouldFetchWeatherReport;
    private String goal;
    private boolean requiresSmileSelfie;
    private Intent mCallingIntent;
    private VibrationManager mVibrationManager;
    private LocationManagerWrapper mLocationManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        mViewModel = new AlarmActivityViewModel(getApplication());
        mVibrationManager = new VibrationManager(this);
        mVibrationManager.vibrateIndefinetly();
        mCallingIntent = getIntent();
        setWindowsFlags();

        goal = mCallingIntent.getExtras().getString("goal");
        requiresSmileSelfie = mCallingIntent.getExtras().getBoolean("dismiss_on_selfie");
        shouldFetchWeatherReport = mCallingIntent.getExtras().getBoolean("get_report");

        initViews();

        if (shouldFetchWeatherReport) {
            handleWeatherUpdates();
        }
    }

    private void getBitmapFromCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(this.getPackageManager()) != null) {
            startActivityForResult(cameraIntent, TAKE_SELFIE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == TAKE_SELFIE) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                mViewModel.fetchSmileProbability(bitmap);
                mViewModel.getSmileProbability().observe(this, aFloat -> {
                    tvSmileProbability.setText("" + aFloat);
                    if (aFloat >= 0.5f) {
                        stopAlarm();
                    } else {
                        if (aFloat == 0.0f) {
                            Toast.makeText(
                                    AlarmActivity.this,
                                    "Processing...",
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                        Toast.makeText(
                                AlarmActivity.this,
                                "Please Smile :)",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                });
            }
        }
    }

    private void handleWeatherUpdates() {
        Location location = LocationManagerWrapper.getLastKnownLocation(this);

        //===HOTFIX===
        double lat = 47.1667;
        double lon = 27.6;

        if (location != null) {
            lat = location.getLatitude();
            lon = location.getLongitude();
        }

        mViewModel.fetchWeatherReport(lat, lon);
        mViewModel.getWeatherReport().observe(this, s -> {
            try {
                updateUi(s);
                mTts = new TextToSpeechWrapper(getApplicationContext(), i -> {
                    if (i == TextToSpeech.SUCCESS && !s.equals("No Data")) {
                        mTts.speak(s);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    private void initViews() {
        ((TextView) findViewById(R.id.currentAlarmName)).setText(
                mCallingIntent.getExtras().getString("name")
        );
        tvWeatherReport = findViewById(R.id.tvWeatherReport);
        tvLocation = findViewById(R.id.tvLocation);
        etEnteredGoal = findViewById(R.id.etAlarmGoal);
        btnSubmit = findViewById(R.id.btnSubmit);
        tvSmileProbability = findViewById(R.id.tvSmileProbability);
        btnSelfie = findViewById(R.id.btnSelfie);
        if (goal != null) {
            btnSubmit.setOnClickListener((l) -> {
                if (isGoalAccurate(etEnteredGoal.getText().toString(), goal)) {
                    stopAlarm();
                }
            });
        } else {
            etEnteredGoal.setVisibility(View.GONE);
            btnSubmit.setVisibility(View.GONE);
        }
        btnSelfie.setOnClickListener((l) -> {
            getBitmapFromCamera();
        });
    }

    @Override
    public void onBackPressed() {
        if (goal.equals("")) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "State your goal", Toast.LENGTH_SHORT).show();
        }
    }

    private void stopAlarm() {
        mVibrationManager.stopVibrating();
        this.finish();
    }

    private void setWindowsFlags() {
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void updateUi(String toDisplay) throws JSONException {
        if (toDisplay.equals("No Data")) {
            //TODO: set loading bar
            return;
        }

        tvWeatherReport.setText(toDisplay);
        tvLocation.setText(Util.getLastLocation());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTts != null) {
            mTts.clear();
        }
    }


    private Boolean isGoalAccurate(String goalToCheck, String actualGoal) {
        return mViewModel.areStringsSimilar(goalToCheck, actualGoal);
    }
}
