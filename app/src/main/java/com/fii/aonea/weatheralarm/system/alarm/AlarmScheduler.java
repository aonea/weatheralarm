package com.fii.aonea.weatheralarm.system.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.fii.aonea.weatheralarm.data.model.Alarm;
import com.fii.aonea.weatheralarm.view.alarm.AlarmActivity;

import java.util.Calendar;

public class AlarmScheduler {

    public static void scheduleAlarm(Context context, Alarm alarm) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmActivity.class);
        intent.putExtra("name", alarm.getName());
        intent.putExtra("get_report", alarm.isWeatherReportOn());
        intent.putExtra("goal", alarm.getGoal());
        intent.putExtra("dismiss_on_selfie", alarm.requiresSmileSelfie());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, alarm.getHour());
        calendar.set(Calendar.MINUTE, alarm.getMinutes());


        if (alarm.isActive()) {
            alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(),
                    alarm.getRepeat(),
                    pendingIntent
            );
        } else {
            //cancel
            alarmManager.cancel(pendingIntent);
        }
    }

}
