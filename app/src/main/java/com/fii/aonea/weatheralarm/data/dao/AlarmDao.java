package com.fii.aonea.weatheralarm.data.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.fii.aonea.weatheralarm.data.model.Alarm;

import java.util.List;

@Dao
public interface AlarmDao {

    @Insert
    void insert(Alarm alarm);

    @Query("select * from alarms order by mHour, mMinutes")
    LiveData<List<Alarm>> getAllAlarms();

    @Query("delete from alarms")
    void dropAlarmsTable();

    @Query("update alarms set isActive = :isActive where mName=:alarmName")
    void setActive(String alarmName, Boolean isActive);

    @Query("update alarms set isWeatherReportOn = :bool where mName=:name")
    void setWeatherReport(String name, boolean bool);
}
